package com.softtek.library.util;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EntityManagerProducer {

	@Produces
    @MySQLDatabase
    public EntityManager createEntityManager() {
		// Manipulate all mapping entities on my DB
        return Persistence
                .createEntityManagerFactory("application")
                .createEntityManager();
    }

    public void close(
            @Disposes @MySQLDatabase EntityManager entityManager) {
        entityManager.close();
    }
}
