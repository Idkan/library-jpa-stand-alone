package com.softtek.library.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.softtek.library.model.Author;
import com.softtek.library.service.AuthorService;

/**
 * Servlet implementation class AuthorController
 */
@WebServlet("/api/v1/author")
public class AuthorController extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
    
	@Inject
	private AuthorService authorService;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthorController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/json");
		List<Author> authors = authorService.getAllAuthors();
		
		JSONObject JSONresponse = new JSONObject();
		JSONArray responseList = new JSONArray(authors);
		
		JSONresponse.put("JSONresponse", responseList);
		
		PrintWriter out = response.getWriter();
		out.print(JSONresponse);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String firstName = request.getParameter("firstName");
		
		Author author = authorService.getAuthorByFirstName(firstName);
		
		JSONObject JSONresponse = new JSONObject();
		JSONObject taskJson = new JSONObject(author);
		
		JSONresponse.put("author", taskJson);
		response.setContentType("text/json");
		
		PrintWriter out = response.getWriter();
		out.print(JSONresponse);
		
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
