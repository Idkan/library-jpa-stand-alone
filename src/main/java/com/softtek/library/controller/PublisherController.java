package com.softtek.library.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.softtek.library.model.Publisher;
import com.softtek.library.service.PublisherService;

/**
 * Servlet implementation class PublisherController
 */

@WebServlet("/api/v1/publisher")
public class PublisherController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PublisherService publisherService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublisherController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/json");
		List<Publisher> publishers = publisherService.getAllPublishers();
		
		JSONObject JSONresponse = new JSONObject();
		JSONArray responseList = new JSONArray(publishers);
		
		JSONresponse.put("JSONresponse", responseList);
		
		PrintWriter out = response.getWriter();
		out.print(JSONresponse);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String pidString = request.getParameter("pid");
		int pid = new Integer(pidString);
		String publisherName = request.getParameter("publisherName");
		
		response.setContentType("text/json");
		
		publisherService.updatePublisher(pid, publisherName);

		List<Publisher> publisherResponse = publisherService.getAllPublishers();
		
		JSONObject JSONresponse = new JSONObject();
		JSONArray responseList = new JSONArray(publisherResponse);
		
		JSONresponse.put("PublisherUpdate", responseList);
		
		PrintWriter out = response.getWriter();
		out.print(JSONresponse);
		
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idString = request.getParameter("pid");
		int pid = new Integer(idString);
		
		publisherService.deletePublisher(pid);
		
		JSONObject JSONresponse = new JSONObject();
		
		JSONresponse.put("deleted", pid);
		response.setContentType("text/json");
		
		PrintWriter out = response.getWriter();
		out.print(JSONresponse);
		
	}

}
