package com.softtek.library.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.softtek.library.model.Book;
import com.softtek.library.service.BookService;


@WebServlet("/api/v1/book")
public class BookController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private BookService bookService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/json");
		List<Book> books = bookService.getAllBooks();
				
		JSONObject response = new JSONObject();
		JSONArray responseList = new JSONArray(books);
	
		response.put("response", responseList);
		
		PrintWriter out = resp.getWriter();
		out.print(response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String esbn = req.getParameter("esbn"); 
		
		Book b = bookService.getBookByESBN(esbn);
		JSONObject response = new JSONObject();
		JSONObject taskJson = new JSONObject(b);
		response.put("book", taskJson);
		resp.setContentType("text/json");
		
		PrintWriter out = resp.getWriter();
		out.print(response);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String idString = req.getParameter("id"); 
		int id = new Integer(idString);
		
		bookService.deleteBook(id);
		
		JSONObject response = new JSONObject();
		response.put("deleted", id);
		resp.setContentType("text/json");
		
		PrintWriter out = resp.getWriter();
		out.print(response);
	}
}
