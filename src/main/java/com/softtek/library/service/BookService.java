package com.softtek.library.service;

import java.util.List;

import com.softtek.library.model.Book;

public interface BookService {

	List<Book> getAllBooks();
	Book getBookById(long id);
	Book getBookByESBN(String esbn);
	Book updateBook(Book b);
	void deleteBook(int id);
}
