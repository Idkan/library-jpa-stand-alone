package com.softtek.library.service;

import java.util.List;

import com.softtek.library.model.Publisher;

public interface PublisherService {

	List<Publisher> getAllPublishers();

	void deletePublisher(int pid);

	void updatePublisher(int pid, String publisherName);


}
