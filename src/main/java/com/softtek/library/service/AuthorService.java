package com.softtek.library.service;

import java.util.List;

import com.softtek.library.model.Author;

public interface AuthorService {

	List<Author> getAllAuthors();

	Author getAuthorByFirstName(String firstName);

}
