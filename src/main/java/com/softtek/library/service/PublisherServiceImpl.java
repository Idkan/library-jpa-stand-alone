package com.softtek.library.service;

import java.util.List;

import javax.inject.Inject;

import com.softtek.library.model.Publisher;
import com.softtek.libray.repository.PublisherRepository;

public class PublisherServiceImpl implements PublisherService {
	
	@Inject
	private PublisherRepository repository;

	@Override
	public List<Publisher> getAllPublishers() {
		// TODO Auto-generated method stub
		List<Publisher> result = repository.getAllPublishers();
		result.forEach(publisher -> {
			publisher.getBooks().forEach(book -> {
				book.setPublisher(null);
				book.setAuthors(null);
			});
		});
		return repository.getAllPublishers();
	}

	@Override
	public void deletePublisher(int pid) {
		// TODO Auto-generated method stub
		repository.removePublisher(pid);
	}

	@Override
	public void updatePublisher(int pid, String publisherName) {
		// TODO Auto-generated method stub
		repository.updatePublisher(pid, publisherName);
	}
	

}
