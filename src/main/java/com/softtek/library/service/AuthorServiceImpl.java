package com.softtek.library.service;

import java.util.List;

import javax.inject.Inject;

import com.softtek.library.model.Author;
import com.softtek.libray.repository.AuthorRepository;

public class AuthorServiceImpl implements AuthorService {

	@Inject
	private AuthorRepository repository;
	
	@Override
	public List<Author> getAllAuthors() {
		List<Author> result = repository.getAllAuthors();
		result.forEach(author -> {
			author.getBooks().forEach(book -> {
				book.setAuthors(null);
				book.setPublisher(null);
			});
		});
		return result;
	}

	@Override
	public Author getAuthorByFirstName(String firstName) {
		return repository.getAuthorByFirstName(firstName);
	}

}
