package com.softtek.library.service;

import java.util.List;

import javax.inject.Inject;

import com.softtek.library.model.Book;
import com.softtek.libray.repository.BookRepository;

public class BookServiceImpl implements BookService {

	@Inject
	private BookRepository repository;
	
	@Override
	public List<Book> getAllBooks() {
		List<Book> result = repository.getAllBooks();
		result.forEach(book -> {
			book.getPublisher().setBooks(null);
			book.getAuthors().forEach(author -> author.setBooks(null));
		});
		return result;
	}

	@Override
	public Book getBookById(long id) {
		return repository.getBookById(id);
	}

	@Override
	public Book updateBook(Book b) {
		return null;
	}

	@Override
	public void deleteBook(int id) {
		 repository.removeBook(id);
	}

	@Override
	public Book getBookByESBN(String esbn) {
		return repository.findBookByESBN(esbn);
	}

	
	
}
