package com.softtek.libray.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.softtek.library.model.Book;
import com.softtek.library.util.MySQLDatabase;

public class BookRepository {

	@Inject @MySQLDatabase
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Book> getAllBooks() {
		Query query = entityManager.createQuery("SELECT b from Book b");
		return (List<Book>) query.getResultList();
	}
	
	public Book getBookById(long id) {
		Book book = entityManager.find(Book.class, id);
		return book;
	}
	
	public Book findBookByESBN(String esbn) {
		TypedQuery<Book> query = entityManager.createQuery(
				"Select b from Book b where b.isbn like :esbn", Book.class);
		query.setParameter("esbn", esbn);
		return query.getSingleResult();
	}
	
	public void removeBook(int id) {
		EntityTransaction tx = entityManager.getTransaction();
        Book b = entityManager.find(Book.class, id);
        if( b != null) {
        	tx.begin();
        	entityManager.remove(b);
        	tx.commit();
        }
    }
}
