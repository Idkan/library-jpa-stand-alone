package com.softtek.libray.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.softtek.library.model.Publisher;
import com.softtek.library.util.MySQLDatabase;

public class PublisherRepository {

	@Inject @MySQLDatabase
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Publisher> getAllPublishers() {
		// TODO Auto-generated method stub
		Query query = entityManager.createQuery("SELECT p from Publisher p");
		return (List<Publisher>) query.getResultList();
	}

	public void removePublisher(int pid) {
		// TODO Auto-generated method stub
		EntityTransaction tx = entityManager.getTransaction();
        Publisher p = entityManager.find(Publisher.class, pid);
        if( p != null) {
        	tx.begin();
        	entityManager.remove(p);
        	tx.commit();
        }
	}

	public void updatePublisher(int pid, String publisherName) {
		// TODO Auto-generated method stub
		EntityTransaction tx = entityManager.getTransaction();
		Publisher p = entityManager.find(Publisher.class, pid);
		if (p != null) {
			tx.begin();
			p.setName(publisherName);
			tx.commit();
		}
	}

	
}
