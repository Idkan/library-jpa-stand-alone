package com.softtek.libray.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.softtek.library.model.Author;
import com.softtek.library.util.MySQLDatabase;

public class AuthorRepository {
	
	@Inject @MySQLDatabase
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Author> getAllAuthors() {
		// TODO Auto-generated method stub
		Query query = entityManager.createQuery("SELECT a from Author a");
		return (List<Author>) query.getResultList();
	}

	public Author getAuthorByFirstName(String firstName) {
		// TODO Auto-generated method stub
		TypedQuery<Author> query  = entityManager.createQuery(
				"Select a from Author a where a.firstName like :firstName", Author.class);
		query.setParameter("firstName", firstName);
		return query.getSingleResult();
	}

}
